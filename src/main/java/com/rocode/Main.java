/*
 * Main.java
 * Rijkaard Orismé <ro.code@yahoo.com>
 * MIT License
 * 19/08/2014 9:40 PM
 */

package com.rocode;

/**
 * @author Rijkaard Orismé
 */
public class Main {
    public static void main(String[] args) {
        sayHello();
    }
    
    /**
     * Say hello method
     */
    public static void sayHello() {
        System.out.println("Hello");
    }
}
